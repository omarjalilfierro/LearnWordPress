Version 1.1
=============
	* Mar 20, 2017
	* Added Masonry JS code instead of css masonry
	* JetPack's infinite scroll support

Version 1.0.2
=============
	* Mar 29, 2016
	* Footer credits on mobile issue fix

Version 1.0.1
=============
	* Feb 29, 2016
	* Sidebar on pages not showing bug fix

Version 1.0.0
=============
	* Feb 25, 2016
	* Initial release