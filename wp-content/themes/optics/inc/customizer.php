<?php
/**
 * Optics Theme Customizer.
 *
 * @package Optics
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function optics_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

}
add_action( 'customize_register', 'optics_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function optics_customize_preview_js() {
	wp_enqueue_script( 'optics_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20130508', true );
}
add_action( 'customize_preview_init', 'optics_customize_preview_js' );

/**
 * Add a placeholder section to show users additional available options
 */
function optics_customizer_upgrade_to_pro( $wp_customize ) {

	if ( optics_is_pro() )
		return;

	$wp_customize->add_panel( 'theme_options', array(
        'title'     => esc_html__( 'Theme Options', 'optics' ),
        'priority'  => 105
    ) );

	$wp_customize->add_setting( 'upgrade_option', array() );
	$wp_customize->add_control( 'upgrade_option', array(
		'label'      => __( 'Upgrade options', 'optics' ),
		'section'    => 'theme_options',
		'settings'   => 'upgrade_option',
		'type'       => 'radio',
		'choices'    => array(
			'left'   => 'left',
			'right'  => 'right',
		),
	));

	$wp_customize->add_setting( 'optics_section_fonts' );

	$wp_customize->add_section( 'optics_section_fonts' , array(
	    'title' => __( 'Typography', 'optics' ),
	    'description' => sprintf(
			__( '%s to enable additional fonts choices on your website.', 'optics' ),
			sprintf(
				'<a href="%1$s" target="_blank">%2$s</a>',
				esc_url( optics_get_pro_link() ),
				__( 'Upgrade to optics Pro', 'optics' )
			)
		),
		'panel'    => 'theme_options',
		'priority' => 5
	));

	// Heading Font Setting
	$wp_customize->add_setting( 'heading_font', array(
		'default'			=> '',
		'transport'         => 'postMessage'
	) );

	// Heading Font Control
	$wp_customize->add_control( 'heading_font', array(
		'label'             => esc_html__( 'Headings Font', 'optics' ),
		'section'           => 'optics_section_fonts',
		'type'              => 'text',
		'settings'          => 'heading_font',
		'description'       => '<select><option selected="selected" disabled="disabled"> ' . esc_html__( "Choose a Google font", "optics" ) . '</option></select>',
		'priority'          => 6,
	) );

	// Body Font Setting
	$wp_customize->add_setting( 'body_font', array(
		'default'			=> ''
	) );

	// Body Font Control
	$wp_customize->add_control( 'body_font', array(
		'label'             => esc_html__( 'Body Font', 'optics' ),
		'section'           => 'optics_section_fonts',
		'type'              => 'text',
		'settings'          => 'body_font',
		'description'       => '<select><option selected="selected" disabled="disabled"> ' . esc_html__( "Choose a Google font", "optics" ) . '</option></select>',
		'priority'          => 7,
	) );

	$wp_customize->add_section( 'optics_colors', array(
		'title'         => esc_html__( 'Color Scheme', 'optics' ),
		'description' => sprintf(
			__( '%s to enable color palettes and individual section colors on your website.', 'optics' ),
			sprintf(
				'<a href="%1$s" target="_blank">%2$s</a>',
				esc_url( optics_get_pro_link() ),
				__( 'Upgrade to optics Pro', 'optics' )
			)
		),
		'priority'      => 10,
		'panel'         => 'theme_options'
	) );

	// Color Scheme Setting
	$wp_customize->add_setting( 'optics_color', array(
		'default'           => 'default'
	) );

	// Color Scheme Control
	$wp_customize->add_control( 'optics_color', array(
		'label'             => esc_html__( 'Color Scheme', 'optics' ),
		'section'           => 'optics_colors',
		'type'              => 'text',
		'settings'          => 'optics_color',
		'description'       => '<select>
									<option selected="selected" disabled="disabled"> ' . esc_html__( "Choose a Color Scheme", "optics" ) . '</option>
									<option disabled="disabled"> ' . esc_html__( "Default", "optics" ) . '</option>
									<option disabled="disabled"> ' . esc_html__( "Beautiful", "optics" ) . '</option>
									<option disabled="disabled"> ' . esc_html__( "Blossom", "optics" ) . '</option>
									<option disabled="disabled"> ' . esc_html__( "Camo", "optics" ) . '</option>
									<option disabled="disabled"> ' . esc_html__( "Dark", "optics" ) . '</option>
									<option disabled="disabled"> ' . esc_html__( "Ice", "optics" ) . '</option>
									<option disabled="disabled"> ' . esc_html__( "Oreo", "optics" ) . '</option>
									<option disabled="disabled"> ' . esc_html__( "Splash", "optics" ) . '</option>
									<option disabled="disabled"> ' . esc_html__( "Spring", "optics" ) . '</option>
								</select>',
	) );

	/**
	 * Mega Menu
	 */
	$wp_customize->add_section( 'optics_megamenu', array(
		'title'         => esc_html__( 'Mega Menu', 'optics' ),
		'description' => sprintf(
			esc_html__( '%s to enable mega menu for your website.', 'optics' ),
			sprintf(
				'<a href="%1$s" target="_blank">%2$s</a>',
				esc_url( optics_get_pro_link() ),
				esc_html__( 'Upgrade to optics Pro', 'optics' )
			)
		),
		'priority'      => 10,
		'panel'         => 'theme_options'
	) );

	// Mega Menu Setting
	$wp_customize->add_setting( 'optics_megamenu', array(
		'default'           => 'default'
	) );

	// Mega Menu Control
	$wp_customize->add_control( 'optics_megamenu', array(
		'label'             => esc_html__( 'Enable Mega Menu', 'optics' ),
		'section'           => 'optics_megamenu',
		'type'              => 'text',
		'settings'          => 'optics_megamenu',
		'description'       => esc_html__( 'This converts existing Primary Menu to a Mega Menu.', 'optics' ) . '<br/><select>
									<option selected="selected" disabled="disabled"> ' . esc_html__( "No", "optics" ) . '</option>
									<option disabled="disabled"> ' . esc_html__( "Yes", "optics" ) . '</option>
								</select>',
	) );

	/**
	 * Galleries
	 */
	$wp_customize->add_section( 'optics_galleries', array(
		'title'         => esc_html__( 'Galleries', 'optics' ),
		'description' => sprintf(
			esc_html__( '%s to enable galleries option for your website. The settings below apply to all galleries that you insert into your Posts and Pages.', 'optics' ),
			sprintf(
				'<a href="%1$s" target="_blank">%2$s</a>',
				esc_url( optics_get_pro_link() ),
				esc_html__( 'Upgrade to optics Pro', 'optics' )
			)
		),
		'priority'      => 10,
		'panel'         => 'theme_options'
	) );

	// Galleries Setting
	$wp_customize->add_setting( 'optics_galleries_tm', array(
		'default'           => '',
	) );

	// Galleries Control
	$wp_customize->add_control( 'optics_galleries', array(
		'label'             => esc_html__( 'Transition Mode', 'optics' ),
		'section'           => 'optics_galleries',
		'type'              => 'text',
		'settings'          => 'optics_galleries_tm',
		'description'       => '<select>
									<option selected="selected" disabled="disabled"> ' . esc_html__( "Slide", "optics" ) . '</option>
									<option disabled="disabled"> ' . esc_html__( "Fade", "optics" ) . '</option>
								</select>',
	) );

	// Galleries Setting
	$wp_customize->add_setting( 'optics_galleries_ts', array(
		'default'           => '',
	) );

	// Galleries Control
	$wp_customize->add_control( 'optics_galleries_ts', array(
		'label'             => esc_html__( 'Transition Speed', 'optics' ),
		'section'           => 'optics_galleries',
		'type'              => 'text',
		'settings'          => 'optics_galleries_ts',
		'description'       => '<input type="text" disabled="disabled" value="5000" />',
	) );

	// Gallery Setting
	$wp_customize->add_setting( 'optics_galleries_id', array(
		'default'           => ''
	) );

	// Gallery Control
	$wp_customize->add_control( 'optics_galleries_id', array(
		'label'             => esc_html__( 'Image Download Icon', 'optics' ),
		'section'           => 'optics_galleries',
		'type'              => 'text',
		'settings'          => 'optics_galleries_id',
		'description'       => '<select>
									<option selected="selected" disabled="disabled"> ' . esc_html__( "Show", "optics" ) . '</option>
									<option disabled="disabled"> ' . esc_html__( "Hide", "optics" ) . '</option>
								</select>',
	) );

	// Gallery Setting
	$wp_customize->add_setting( 'optics_galleries_ic', array(
		'default'           => ''
	) );

	// Gallery Control
	$wp_customize->add_control( 'optics_galleries_ic', array(
		'label'             => esc_html__( 'Image Counter', 'optics' ),
		'section'           => 'optics_galleries',
		'type'              => 'text',
		'settings'          => 'optics_galleries_ic',
		'description'       => '<select>
									<option selected="selected" disabled="disabled"> ' . esc_html__( "Show", "optics" ) . '</option>
									<option disabled="disabled"> ' . esc_html__( "Hide", "optics" ) . '</option>
								</select>',
	) );

	// Gallery Setting
	$wp_customize->add_setting( 'optics_galleries_t', array(
		'default'           => ''
	) );

	// Gallery Control
	$wp_customize->add_control( 'optics_galleries_t', array(
		'label'             => esc_html__( 'Thumbnails', 'optics' ),
		'section'           => 'optics_galleries',
		'type'              => 'text',
		'settings'          => 'optics_galleries_t',
		'description'       => '<select>
									<option selected="selected" disabled="disabled"> ' . esc_html__( "Show", "optics" ) . '</option>
									<option disabled="disabled"> ' . esc_html__( "Hide", "optics" ) . '</option>
								</select>',
	) );

	// Gallery Setting
	$wp_customize->add_setting( 'optics_galleries_ap', array(
		'default'           => ''
	) );

	// Gallery Control
	$wp_customize->add_control( 'optics_galleries_ap', array(
		'label'             => esc_html__( 'Autoplay Option', 'optics' ),
		'section'           => 'optics_galleries',
		'type'              => 'text',
		'settings'          => 'optics_galleries_ap',
		'description'       => '<select>
									<option selected="selected" disabled="disabled"> ' . esc_html__( "Show", "optics" ) . '</option>
									<option disabled="disabled"> ' . esc_html__( "Hide", "optics" ) . '</option>
								</select>',
	) );

	// Gallery Setting
	$wp_customize->add_setting( 'optics_galleries_f', array(
		'default'           => ''
	) );

	// Gallery Control
	$wp_customize->add_control( 'optics_galleries_f', array(
		'label'             => esc_html__( 'Fullscreen Option', 'optics' ),
		'section'           => 'optics_galleries',
		'type'              => 'text',
		'settings'          => 'optics_galleries_f',
		'description'       => '<select>
									<option selected="selected" disabled="disabled"> ' . esc_html__( "Show", "optics" ) . '</option>
									<option disabled="disabled"> ' . esc_html__( "Hide", "optics" ) . '</option>
								</select>',
	) );

	// Gallery Setting
	$wp_customize->add_setting( 'optics_galleries_z', array(
		'default'           => ''
	) );

	// Gallery Control
	$wp_customize->add_control( 'optics_galleries_z', array(
		'label'             => esc_html__( 'Zoom Option', 'optics' ),
		'section'           => 'optics_galleries',
		'type'              => 'text',
		'settings'          => 'optics_galleries_z',
		'description'       => '<select>
									<option selected="selected" disabled="disabled"> ' . esc_html__( "Show", "optics" ) . '</option>
									<option disabled="disabled"> ' . esc_html__( "Hide", "optics" ) . '</option>
								</select>',
	) );


	/**
	 * Featured Content Slider
	 */
	$wp_customize->add_section( 'optics_fcs', array(
		'title'         => esc_html__( 'Featured Content Slider', 'optics' ),
		'description' => sprintf(
			esc_html__( '%s to enable featured content slider option for your website.', 'optics' ),
			sprintf(
				'<a href="%1$s" target="_blank">%2$s</a>',
				esc_url( optics_get_pro_link() ),
				esc_html__( 'Upgrade to optics Pro', 'optics' )
			)
		),
		'priority'      => 10,
		'panel'         => 'theme_options'
	) );

	// FCS Setting
	$wp_customize->add_setting( 'optics_fcs_enable', array(
		'default'           => '',
	) );

	// FCS Control
	$wp_customize->add_control( 'optics_fcs_enable', array(
		'label'             => esc_html__( 'Enable Slider', 'optics' ),
		'section'           => 'optics_fcs',
		'type'              => 'text',
		'settings'          => 'optics_fcs_enable',
		'description'       => '<select>
									<option selected="selected" disabled="disabled"> ' . esc_html__( "Yes", "optics" ) . '</option>
									<option disabled="disabled"> ' . esc_html__( "No", "optics" ) . '</option>
								</select>',
	) );

	// FCS Setting
	$wp_customize->add_setting( 'optics_fcs_st', array(
		'default'           => '',
	) );

	// FCS Control
	$wp_customize->add_control( 'optics_fcs_st', array(
		'label'             => esc_html__( 'Slider Thumbnails', 'optics' ),
		'section'           => 'optics_fcs',
		'type'              => 'text',
		'settings'          => 'optics_fcs_st',
		'description'       => '<select>
									<option selected="selected" disabled="disabled"> ' . esc_html__( "Show", "optics" ) . '</option>
									<option disabled="disabled"> ' . esc_html__( "Hide", "optics" ) . '</option>
								</select>',
	) );

	// FCS Setting
	$wp_customize->add_setting( 'optics_fcs_si', array(
		'default'           => ''
	) );

	// FCS Control
	$wp_customize->add_control( 'optics_fcs_si', array(
		'label'             => esc_html__( 'Slider Items', 'optics' ),
		'section'           => 'optics_fcs',
		'type'              => 'text',
		'settings'          => 'optics_fcs_si',
		'description'       => '<select>
									<option disabled="disabled"> ' . esc_html__( "1", "optics" ) . '</option>
									<option disabled="disabled"> ' . esc_html__( "2", "optics" ) . '</option>
									<option selected="selected" disabled="disabled"> ' . esc_html__( "3", "optics" ) . '</option>
									<option disabled="disabled"> ' . esc_html__( "4", "optics" ) . '</option>
									<option disabled="disabled"> ' . esc_html__( "5", "optics" ) . '</option>
									<option disabled="disabled"> ' . esc_html__( "6", "optics" ) . '</option>
									<option disabled="disabled"> ' . esc_html__( "7", "optics" ) . '</option>
									<option disabled="disabled"> ' . esc_html__( "8", "optics" ) . '</option>
									<option disabled="disabled"> ' . esc_html__( "9", "optics" ) . '</option>
									<option disabled="disabled"> ' . esc_html__( "10", "optics" ) . '</option>
								</select>',
	) );

	// FCS Setting
	$wp_customize->add_setting( 'optics_fcs_stm', array(
		'default'           => ''
	) );

	// FCS Control
	$wp_customize->add_control( 'optics_fcs_stm', array(
		'label'             => esc_html__( 'Slider Transition Mode', 'optics' ),
		'section'           => 'optics_fcs',
		'type'              => 'text',
		'settings'          => 'optics_fcs_stm',
		'description'       => '<select>
									<option selected="selected" disabled="disabled"> ' . esc_html__( "Fade", "optics" ) . '</option>
									<option disabled="disabled"> ' . esc_html__( "Slide", "optics" ) . '</option>
								</select>',
	) );

	// FCS Setting
	$wp_customize->add_setting( 'optics_fcs_sat', array(
		'default'           => ''
	) );

	// FCS Control
	$wp_customize->add_control( 'optics_fcs_sat', array(
		'label'             => esc_html__( 'Slider Auto Start', 'optics' ),
		'section'           => 'optics_fcs',
		'type'              => 'text',
		'settings'          => 'optics_fcs_sat',
		'description'       => '<select>
									<option selected="selected" disabled="disabled"> ' . esc_html__( "Yes", "optics" ) . '</option>
									<option disabled="disabled"> ' . esc_html__( "No", "optics" ) . '</option>
								</select>',
	) );

	// FCS Setting
	$wp_customize->add_setting( 'optics_fcs_sh', array(
		'default'           => ''
	) );

	// FCS Control
	$wp_customize->add_control( 'optics_fcs_sh', array(
		'label'             => esc_html__( 'Slider Height', 'optics' ),
		'section'           => 'optics_fcs',
		'type'              => 'text',
		'settings'          => 'optics_fcs_sh',
		'description'       => '<select>
									<option selected="selected" disabled="disabled"> ' . esc_html__( "Adaptive", "optics" ) . '</option>
									<option disabled="disabled"> ' . esc_html__( "Fixed", "optics" ) . '</option>
								</select>',
	) );

	// FCS Setting
	$wp_customize->add_setting( 'optics_fcs_vl', array(
		'default'           => ''
	) );

	// FCS Control
	$wp_customize->add_control( 'optics_fcs_vl', array(
		'label'             => esc_html__( 'Vertical Layout', 'optics' ),
		'section'           => 'optics_fcs',
		'type'              => 'text',
		'settings'          => 'optics_fcs_vl',
		'description'       => '<select>
									<option selected="selected" disabled="disabled"> ' . esc_html__( "Yes", "optics" ) . '</option>
									<option disabled="disabled"> ' . esc_html__( "No", "optics" ) . '</option>
								</select>',
	) );


	/**
	 * Custom CSS Section
	 */
	$wp_customize->add_section( 'custom_css', array(
		'title'         => esc_html__( 'Custom CSS', 'optics' ),
		'description' => sprintf(
			__( '%s to enable Custom CSS on your website.', 'optics' ),
			sprintf(
				'<a href="%1$s" target="_blank">%2$s</a>',
				esc_url( optics_get_pro_link() ),
				__( 'Upgrade to optics Pro', 'optics' )
			)
		),
		'priority'      => 15,
		'panel'         => 'theme_options'
	) );

	// Custom CSS Setting
	$wp_customize->add_setting( 'css', array(
		'default'       => '',
	) );

	// Custom CSS Control
	$wp_customize->add_control( 'css', array(
		'section'       => 'custom_css',
	) );

	$wp_customize->add_section( 'credits', array(
		'title'         => esc_html__( 'Footer Credits', 'optics' ),
		'description' => sprintf(
			__( '%s to disable or add custom footer credits on your website.', 'optics' ),
			sprintf(
				'<a href="%1$s" target="_blank">%2$s</a>',
				esc_url( optics_get_pro_link() ),
				__( 'Upgrade to optics Pro', 'optics' )
			)
		),
		'priority'      => 20,
		'panel'         => 'theme_options'
	) );

	// Default Copyright Setting
	$wp_customize->add_setting( 'default_credits', array(
		'default'			=> 'show'
	) );

	// Default Copyright Control
	$wp_customize->add_control( 'default_credits', array(
		'label'             => esc_html__( 'Default Footer Credits', 'optics' ),
		'section'           => 'credits',
		'type'              => 'text',
		'settings'          => 'default_credits',
		'description'       => '<select>
									<option selected="selected" disabled="disabled"> ' . esc_html__( "Show", "optics" ) . '</option>
									<option disabled="disabled"> ' . esc_html__( "Hide", "optics" ) . '</option>
								</select>',

	) );

	// Custom Copyright Setting
	$wp_customize->add_setting( 'custom_credits', array() );

	// Custom Copyright Control
	$wp_customize->add_control( 'custom_credits', array(
		'label'             => esc_html__( 'Custom Footer Credits', 'optics' ),
		'description'		=> esc_html__( 'Add your own credits', 'optics' ) . 
								'<input type="text" disabled="disabled" />',
		'section'           => 'credits',
		'type'              => 'text',
		'settings'          => 'custom_credits',
	) );

}
add_action( 'customize_register', 'optics_customizer_upgrade_to_pro' );

/**
 * Localize scripts for Customizer
 * @return [type] [description]
 */
function optics_customizer_scripts() {

	if ( ! optics_is_pro() ) {
		wp_enqueue_style( 'optics-customizer-sections', get_template_directory_uri() . "/inc/css/customizer-sections.css", '', '20130508' );
		wp_enqueue_script( 'optics-customizer-sections',	get_template_directory_uri() . '/js/customizer-scripts.js', '', '20160108', true );

		// Add localization strings
	
		$data = array(
			'proURL'	=> esc_url( optics_get_pro_link( 'customize-head' ) ),
			'proLabel'	=> esc_html__( 'Upgrade to Optics Pro', 'optics' ),
		);
		// Localize the script
		wp_localize_script(
			'optics-customizer-sections',
			'opticsPro',
			$data
		);
	}

}
add_action( 'customize_controls_enqueue_scripts', 'optics_customizer_scripts' );