/**
 * customizer-scripts.js
 *
 * Theme Customizer enhancements for a better user experience.
 *
 * Contains handlers to make Theme Customizer preview reload changes asynchronously.
 */

( function( $ ) {

	// Add Optics Pro message
	if ('undefined' !== typeof opticsPro) {
		upgrade = $('<a class="optics-customize-pro"></a>')
			.attr('href', opticsPro.proURL)
			.attr('target', '_blank')
			.text(opticsPro.proLabel)
		;
		$('.preview-notice').append(upgrade);
		// Remove accordion click event
		$('.optics-customize-pro').on('click', function(e) {
			e.stopPropagation();
		});
	}

} )( jQuery );
