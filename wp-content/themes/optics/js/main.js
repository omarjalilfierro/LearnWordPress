jQuery(document).ready(function($){

    // Menu interactions
    $( '#primary-menu li:has(ul)' ).addClass( 'parent' ).append( '<span class="genericon genericon-expand caret"></span>' );
    $( '#primary-menu span' ).click( function() {
        $( this ).prev( '.sub-menu' ).toggleClass( 'open' );
        $( this ).toggleClass( 'focus' );
    });

    var $container;

	function optics_trigger_masonry() {
	    // don't proceed if $grid has not been selected
	    if ( ! $container ) {
	        return;
	    }
	    // init Masonry
	    $container.imagesLoaded( function() {
	        $container.masonry({
	            // options
	            itemSelector: 'article.jetpack-portfolio',
	            percentPosition: true,
	            gutter: 20,
	            hiddenStyle: { opacity: 0 }
	        });
	    });
	}

	$( window ).load( function() {
	    $container = $( '.portfolio-content' ); // this is the grid container

	    optics_trigger_masonry();

	    // Triggers re-layout on infinite scroll
	    $( document.body ).on( 'post-load', function () {
	        
	        // I removed the infinite_count code
	        var $selector = $( '.infinite-wrap' );
	        var $elements = $selector.find( '.jetpack-portfolio' );
	        
	        /* here is the idea which is to catch the selector whether it contain element or not, if it's move it to the masonry grid. */
	        if ( $selector.children().length > 0 ) {
	            $container.append( $elements ).masonry( 'appended', $elements, true );
	            optics_trigger_masonry();
	        }
	        
	    });
	});

});