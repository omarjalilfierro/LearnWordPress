== Optics ==

Optics is a clean, minimalist Portfolio theme with a vertical menu and a masonry-like gallery layout. Use this theme for your portfolio, blog, or online store.

=== Installation ===

1. Log in to your WordPress dashboard. This can be found at yourdomain.com/wp-admin
2. Go to Appearance -> Themes and click on the **Install Themes** tab.
3. Click on the **Upload** link.
4. Upload the zip file that you downloaded and click **Install Now**.
5. Click **Activate**.

![Installing yoru theme - Step 1](http://graphpaperpress.s3.amazonaws.com/images/instructions/theme-install-add-new.jpg)
![Installing yoru theme - Step 2](http://graphpaperpress.s3.amazonaws.com/images/instructions/theme-install-upload-theme.jpg)
![Installing yoru theme - Step 3](http://graphpaperpress.s3.amazonaws.com/images/instructions/theme-install-choose-file.jpg)

=== Recommended Plugins ===


====== JetPack ======

The JetPack plugin is required in order to fully utilize the features of this theme. Key JetPack-supported features include: JetPack Custom Post Type � Portfolio, Tiled Galleries, Site Logo, and Infinite Scroll.

====== Sell Media ======

Sell Media allows you to sell photos, prints and downloads directly from your WordPress website.

=== Demo Content ===

Optional: You can download a demo content file [here](http://graphpaperpress-demo-content.s3.amazonaws.com/optics.xml) that you can import in Tools -> Import.
In many cases just importing the demo content will not work since you will also have to make some manual configurations. Please go through this documentation to fully understand how the theme works in order to customize it to fit your needs.


=== Setting Up Homepage Like The Demo ===

**Install Jetpack**

Please refer to [Jetpack's offical documentation](https://jetpack.com/support/installing-jetpack/) for installation instructions.
Once Jetpack is installed and active:

- Activate the **Custom Content Types** and the **Tiled Galleries** modules in your Jetpack settings


**Set the homepage**

1. Create several Portfolio Posts by going to Portfolio -> Add New.

2. Visit Pages -> Add new and create a new page called **Portfolio** (or any other relevant term) and set the Page Template to **Default Portfolio Template.** You only have to enter the Title and choose the page template.

![Sider Portfolio Page Template](http://graphpaperpress.s3.amazonaws.com/images/instructions/sider-portfolio-page-template.jpg)

4. Go to Settings -> Reading. Under the Front Page Displays option, select a static page option. Then, select your **Portfolio** page as **Front page**.


=== Setting up the rest of the site ====


====== Blog page ======

1. Create a page and call it **Blog** or any other relevant term. This will be used to show your normal posts.

2. Go to Settings -> Reading. Under the Front Page Displays option, select your **Blog** page as **Posts page**.

![Shutterbox Blog Page](http://graphpaperpress.s3.amazonaws.com/images/instructions/shutterbox-blog-page.jpg)
![Shutterbox Reading Settings](http://graphpaperpress.s3.amazonaws.com/images/instructions/optics-reading-settings.jpg)
Now, you should have a homepage that looks like the demo, and a Blog page with normal posts. You can also add the Blog page in the menu for easy access.

If you don't want to use JetPack to show portfolios, you can simply create a page and insert a gallery of images.

====== Store page ======

**Install Sell Media** (optional)

Please refer to [Sell Media's offical documentation](http://graphpaperpress.com/docs/sell-media/) for installation instructions.

1. Create several Sell Media Posts by going to Sell Media -> Add New.

2. Visit Pages -> Add new and create a new page called **Shop** (or any other relevant term) and set the Page Template to **Full Page Template.** In the content area enter the Sell Media All Items shortcode: **[sell_media_all_items size="medium"]**.

![Sider Portfolio Page Template](http://graphpaperpress.s3.amazonaws.com/images/instructions/optics-shop-page.jpg)


=== Menus ===

To learn how to use Menus, please see this [video](https://graphpaperpress.com/support/building-custom-menus/).

This theme supports three menu locations:

1. Primary Menu (in sidebar)
2. Top Menu (top right)
3. Social Menu (in footer above the credits)

Here is how we recommend you setup your menus:

1. To Top Menu does not support submenus.  It also keeps all links visible in mobile screens so only use it only for the most important pages and sections on your website.

2. Use the Primary menu for all other links on your site.  If you want you can rely solely in this menu and not use the Top Menu at all.

3. Use the Social menu for social media links. Give each link a name and paste the urls to your social media websites.  The theme will automatically convert the link to an icon for these sites: WordPress, Facebook, Twitter, Dribbble, Google Plus, Pinterest, Github, Tumblr, Youtube, Flickr, Vimeo, Instagram, Codepen and LinkedIn.

=== Featured Images ===

If you are using the Portfolio Content Type from JetPack as we recommend, you will want to make sure to set a Featured Image for all entries.

=== Changelog ===

**1.0 - February 24, 2016** - Initial release

=== Credits ===

Based on Underscores http://underscores.me/, (C) 2012-2015 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)

normalize.css http://necolas.github.io/normalize.css/, (C) 2012-2015 Nicolas Gallagher and Jonathan Neal, [MIT](http://opensource.org/licenses/MIT)